package com.foxyteam.zaimbezotkazov.fragment

import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.widget.TextView
import com.foxyteam.zaimbezotkazov.R
import com.foxyteam.zaimbezotkazov.activity.MainActivity


abstract class BaseFragment : Fragment() {

    protected lateinit var mainActivity: MainActivity

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainActivity = activity as MainActivity
    }


    fun showProgress(show: Boolean) {
        mainActivity.progressView!!.visibility = if (show) View.VISIBLE else View.GONE
    }


    fun replaceFragment(fragment: Fragment, addToBackStack: Boolean = true, tag: String? = null) {
        val ft = mainActivity.supportFragmentManager.beginTransaction()
        if(tag == null)
            ft.replace(R.id.placeholder, fragment)
        else
            ft.replace(R.id.placeholder, fragment, tag)
        if (addToBackStack) {
            ft.addToBackStack(null)
        }

        ft.commit()
    }

    fun goBack(){
        if (mainActivity.supportFragmentManager.backStackEntryCount > 0) {
            mainActivity.supportFragmentManager.popBackStackImmediate()
        }
    }

    fun changeFontInTextView(view: TextView){
        val type = Typeface.createFromAsset(context?.assets, "font/opensans_regular.ttf")
        view.typeface = type
    }

    fun changeFontInTextViewBold(view: TextView){
        val type = Typeface.createFromAsset(context?.assets, "font/opensans_bold.ttf")
        view.typeface = type
    }
}
