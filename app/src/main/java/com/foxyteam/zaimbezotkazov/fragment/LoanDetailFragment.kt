package com.foxyteam.zaimbezotkazov.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.foxyteam.zaimbezotkazov.R
import kotlinx.android.synthetic.main.loan_details_fragment.*
import android.content.Intent
import android.net.Uri


class LoanDetailFragment : BaseFragment() {

    private var loanUrl: String? = null
    private var loanTitle: String? = null
    private var loanMaxValue: String? = null
    private var loanMaxTerm: String? = null
    private var loanPercValue: String? = null
    private var loanObtaineWay: String? = null
    private var loanObtaineTime: String? = null
    private var loanRequirements: String? = null
    private var loanLocation: String? = null
    private var loanImg: Int? = null

    override fun onStart() {
        super.onStart()

        loanUrl = arguments?.getString(ARG_URL)
        loanTitle = arguments?.getString(ARG_TITLE)
        loanMaxValue = arguments?.getString(ARG_MAX_VALUE)
        loanMaxTerm = arguments?.getString(ARG_MAX_TERM)
        loanPercValue = arguments?.getString(ARG_PERC_VALUE)
        loanObtaineWay = arguments?.getString(ARG_OBTAINE_WAY)
        loanObtaineTime = arguments?.getString(ARG_OBTAINE_TIME)
        loanRequirements = arguments?.getString(ARG_REQUIREMENTS)
        loanLocation = arguments?.getString(ARG_LOCATION)
        loanImg = arguments?.getInt(ARG_IMAGE)

        if (loanLocation == "empty"){
            locationLayout.visibility = View.GONE
            locationView.visibility = View.GONE
        } else {
            locationLayout.visibility = View.VISIBLE
            locationView.visibility = View.VISIBLE
        }


        time.text = loanMaxTerm
        textSumma.text = loanMaxValue
        percentage.text = loanPercValue
        obtainWay.text = loanObtaineWay
        obtaineTime.text = loanObtaineTime
        requirements.text = loanRequirements
        location.text = loanLocation

        Glide.with(context!!).load(loanImg).into(loanImage)

        changeFontInTextView(text)
        changeFontInTextView(text2)
        changeFontInTextView(text3)
        changeFontInTextView(text4)
        changeFontInTextView(text5)
        changeFontInTextView(text6)
        changeFontInTextView(text7)
        changeFontInTextView(textDetails)


        changeFontInTextViewBold(time)
        changeFontInTextViewBold(textSumma)
        changeFontInTextViewBold(percentage)
        changeFontInTextViewBold(obtainWay)
        changeFontInTextViewBold(obtaineTime)
        changeFontInTextViewBold(requirements)
        changeFontInTextViewBold(location)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.loan_details_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textDetails.setOnClickListener {

            val formatted = getString(R.string.url_string, loanUrl)
            val url = formatted
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }

    companion object {

        val ARG_URL = "arg_url"
        val ARG_TITLE = "arg_title"
        val ARG_MAX_VALUE = "arg_max_value"
        val ARG_MAX_TERM = "arg_max_term"
        val ARG_PERC_VALUE = "arg_perc_value"
        val ARG_OBTAINE_WAY = "arg_obtaine_way"
        val ARG_OBTAINE_TIME = "arg_obtaine_time"
        val ARG_REQUIREMENTS = "arg_requirements"
        val ARG_LOCATION = "arg_location"
        val ARG_IMAGE = "arg_image"

        fun newInstance(url: String, title: String, maxValue: String, maxTerm: String, percValue: String,
                        obtaineWay: String, obtaineTime: String, requirements: String, location: String, image: Int):
                LoanDetailFragment
        {

            val args = Bundle()

            args.putString(ARG_URL, url)
            args.putString(ARG_TITLE, title)
            args.putString(ARG_MAX_VALUE, maxValue)
            args.putString(ARG_MAX_TERM, maxTerm)
            args.putString(ARG_PERC_VALUE, percValue)
            args.putString(ARG_OBTAINE_WAY, obtaineWay)
            args.putString(ARG_OBTAINE_TIME, obtaineTime)
            args.putString(ARG_REQUIREMENTS, requirements)
            args.putString(ARG_LOCATION, location)
            args.putInt(ARG_IMAGE, image)
            val fragment = LoanDetailFragment()
            fragment.arguments = args
            return fragment
        }
    }
}