package com.foxyteam.zaimbezotkazov.activity

import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import com.foxyteam.zaimbezotkazov.R
import com.foxyteam.zaimbezotkazov.fragment.MainFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var progressView: FrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progressView = findViewById(R.id.progressMain)

        val type = Typeface.createFromAsset(assets, "font/12670.otf")
        textToolbar.typeface = type

        if (savedInstanceState == null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.placeholder, MainFragment())
            ft.commit()
        }
    }
}
