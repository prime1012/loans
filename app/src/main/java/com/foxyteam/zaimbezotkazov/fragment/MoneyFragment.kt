package com.foxyteam.zaimbezotkazov.fragment

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.foxyteam.zaimbezotkazov.R
import com.foxyteam.zaimbezotkazov.adapter.LoansAdapter
import com.foxyteam.zaimbezotkazov.model.LoanModel
import kotlinx.android.synthetic.main.main_fragment.*


class MainFragment : BaseFragment() {

    private lateinit var allAdapter: LoansAdapter
    private lateinit var freeAdapter: LoansAdapter

    private var listAll: ArrayList<LoanModel> = arrayListOf()
    private var listFree: ArrayList<LoanModel> = arrayListOf()

    override fun onStart() {
        super.onStart()

        changeFontInTextView(textFree)
        changeFontInTextViewBold(textAll)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.main_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fillAllList()
        fillFreeList()

        val linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        val linearLayoutManager2 = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        freeAdapter = LoansAdapter{
            url, title, maxValue, maxTerm, percValue, obtaineWay, obtaineTime, requirements, location, image ->
            replaceFragment(LoanDetailFragment.newInstance(url!!, title!!, maxValue!!, maxTerm!!, percValue!!, obtaineWay!!, obtaineTime!!, requirements!!, location!!, image!!), true)
        }

        loansFree.layoutManager = linearLayoutManager
//        loansFree.hasFixedSize()
//        loansFree.isNestedScrollingEnabled = true
        loansFree.adapter = freeAdapter
        freeAdapter.setList(listFree)

        allAdapter = LoansAdapter{
            url, title, maxValue, maxTerm, percValue, obtaineWay, obtaineTime, requirements, location, image ->
            replaceFragment(LoanDetailFragment.newInstance(url!!, title!!, maxValue!!, maxTerm!!, percValue!!, obtaineWay!!, obtaineTime!!, requirements!!, location!!, image!!), true)
        }
        allLoans.layoutManager = linearLayoutManager2
//        allLoans.hasFixedSize()
//        allLoans.isNestedScrollingEnabled = true
        allLoans.adapter = allAdapter
        allAdapter.setList(listAll)

        textAll.setOnClickListener {
           if (allLoans.visibility == View.VISIBLE){
               return@setOnClickListener
           } else {
               freeLayout.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
               allLayout.setBackgroundColor(ContextCompat.getColor(context!!, R.color.green_button))
               textFree.setTextColor(ContextCompat.getColor(context!!, R.color.black))
               textAll.setTextColor(ContextCompat.getColor(context!!, R.color.white))
               allLoans.visibility = View.VISIBLE
               loansFree.visibility = View.GONE
               changeFontInTextViewBold(textAll)
               changeFontInTextView(textFree)
               allLayout.visibility = View.GONE
               freeLayout.visibility = View.VISIBLE
           }
        }

        textFree.setOnClickListener {
           if (loansFree.visibility == View.VISIBLE){
               return@setOnClickListener
           } else {
               freeLayout.setBackgroundColor(ContextCompat.getColor(context!!, R.color.green_button))
               textFree.setTextColor(ContextCompat.getColor(context!!, R.color.white))
               textAll.setTextColor(ContextCompat.getColor(context!!, R.color.black))
               allLayout.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
               loansFree.visibility = View.VISIBLE
               allLoans.visibility = View.GONE
               changeFontInTextViewBold(textFree)
               changeFontInTextView(textAll)
               freeLayout.visibility = View.GONE
               allLayout.visibility = View.VISIBLE
           }
        }
    }

    private fun fillFreeList(){

        listFree.clear()

        listFree.add(0, LoanModel(getString(R.string.moneyman_url), getString(R.string.moneyman_title),
                getString(R.string.moneyman_max), getString(R.string.moneyman_term), getString(R.string.moneyman_percentage),
                getString(R.string.moneyman_way), getString(R.string.moneyman_time), getString(R.string.moneyman_req), "empty", R.drawable.logo_moneyman)
        )

        listFree.add(1, LoanModel(getString(R.string.chestnoeslovo_url), getString(R.string.chestnoeslovo_title),
                getString(R.string.chestnoeslovo_max), getString(R.string.chestnoeslovo_term), getString(R.string.chestnoeslovo_percentage),
                getString(R.string.chestnoeslovo_way), getString(R.string.chestnoeslovo_time), getString(R.string.chestnoeslovo_req), "empty", R.drawable.logo_slovo)
        )

        listFree.add(2, LoanModel(getString(R.string.ezaem_url), getString(R.string.ezaem_title),
                getString(R.string.ezaem_max), getString(R.string.ezaem_term), getString(R.string.ezaem_percentage),
                getString(R.string.ezaem_way), getString(R.string.ezaem_time), getString(R.string.ezaem_req), "empty", R.drawable.logo_ezaem)
        )

        listFree.add(3, LoanModel(getString(R.string.ekapusta_url), getString(R.string.ekapusta_title),
                getString(R.string.ekapusta_max), getString(R.string.ekapusta_term), getString(R.string.ekapusta_percentage),
                getString(R.string.ekapusta_way), getString(R.string.ekapusta_time), getString(R.string.ekapusta_req), "empty", R.drawable.logo_ekapusta)
        )

        listFree.add(4, LoanModel(getString(R.string.centrzaimov_url), getString(R.string.centrzaimov_title),
                getString(R.string.centrzaimov_max), getString(R.string.centrzaimov_term), getString(R.string.centrzaimov_percentage),
                getString(R.string.centrzaimov_way), getString(R.string.centrzaimov_time), getString(R.string.centrzaimov_req), getString(R.string.centrzaimov_location), R.drawable.logo_centrzaimov)
        )

        listFree.add(5, LoanModel(getString(R.string.webbankir_url), getString(R.string.webbankir_title),
                getString(R.string.webbankir_max), getString(R.string.webbankir_term), getString(R.string.webbankir_percentage),
                getString(R.string.webbankir_way), getString(R.string.webbankir_time), getString(R.string.webbankir_req), "empty", R.drawable.logo_webbankir)
        )
    }

    private fun fillAllList(){
        listAll.clear()
        listAll.add(0, LoanModel(getString(R.string.zaymer_url), getString(R.string.zaymer_title),
                getString(R.string.zaymer_max), getString(R.string.zaymer_term), getString(R.string.zaymer_percentage),
                getString(R.string.zaymer_way), getString(R.string.zaymer_time), getString(R.string.zaymer_req), "empty", R.drawable.logo_zaymer)
        )

        listAll.add(1, LoanModel(getString(R.string.moneyman_url), getString(R.string.moneyman_title),
                getString(R.string.moneyman_max), getString(R.string.moneyman_term), getString(R.string.moneyman_percentage),
                getString(R.string.moneyman_way), getString(R.string.moneyman_time), getString(R.string.moneyman_req), "empty", R.drawable.logo_moneyman)
        )

        listAll.add(2, LoanModel(getString(R.string.chestnoeslovo_url), getString(R.string.chestnoeslovo_title),
                getString(R.string.chestnoeslovo_max), getString(R.string.chestnoeslovo_term), getString(R.string.chestnoeslovo_percentage),
                getString(R.string.chestnoeslovo_way), getString(R.string.chestnoeslovo_time), getString(R.string.chestnoeslovo_req), "empty", R.drawable.logo_slovo)
        )

        listAll.add(3, LoanModel(getString(R.string.kredito24_url), getString(R.string.kredito24_title),
                getString(R.string.kredito24_max), getString(R.string.kredito24_term), getString(R.string.kredito24_percentage),
                getString(R.string.kredito24_way), getString(R.string.kredito24_time), getString(R.string.kredito24_req), "empty", R.drawable.logo_kredito24)
        )

        listAll.add(4, LoanModel(getString(R.string.ezaem_url), getString(R.string.ezaem_title),
                getString(R.string.ezaem_max), getString(R.string.ezaem_term), getString(R.string.ezaem_percentage),
                getString(R.string.ezaem_way), getString(R.string.ezaem_time), getString(R.string.ezaem_req), "empty", R.drawable.logo_ezaem)
        )

        listAll.add(5, LoanModel(getString(R.string.konga_url), getString(R.string.konga_title),
                getString(R.string.konga_max), getString(R.string.konga_term), getString(R.string.konga_percentage),
                getString(R.string.konga_way), getString(R.string.konga_time), getString(R.string.konga_req), "empty", R.drawable.logo_konga)
        )

        listAll.add(6, LoanModel(getString(R.string.moneza_url), getString(R.string.moneza_title),
                getString(R.string.moneza_max), getString(R.string.moneza_term), getString(R.string.moneza_percentage),
                getString(R.string.moneza_way), getString(R.string.moneza_time), getString(R.string.moneza_req), "empty", R.drawable.logo_moneza)
        )

        listAll.add(7, LoanModel(getString(R.string.ekapusta_url), getString(R.string.ekapusta_title),
                getString(R.string.ekapusta_max), getString(R.string.ekapusta_term), getString(R.string.ekapusta_percentage),
                getString(R.string.ekapusta_way), getString(R.string.ekapusta_time), getString(R.string.ekapusta_req), "empty", R.drawable.logo_ekapusta)
        )

        listAll.add(8, LoanModel(getString(R.string.centrzaimov_url), getString(R.string.centrzaimov_title),
                getString(R.string.centrzaimov_max), getString(R.string.centrzaimov_term), getString(R.string.centrzaimov_percentage),
                getString(R.string.centrzaimov_way), getString(R.string.centrzaimov_time), getString(R.string.centrzaimov_req), getString(R.string.centrzaimov_location), R.drawable.logo_centrzaimov)
        )

        listAll.add(9, LoanModel(getString(R.string.webbankir_url), getString(R.string.webbankir_title),
                getString(R.string.webbankir_max), getString(R.string.webbankir_term), getString(R.string.webbankir_percentage),
                getString(R.string.webbankir_way), getString(R.string.webbankir_time), getString(R.string.webbankir_req), "empty", R.drawable.logo_webbankir)
        )
    }

//    companion object {
//
//        val ARG_FOR_MAP = "arg_for_map"
//
//        fun newInstance(isForMap: Boolean): MoneyFragment {
//
//            val args = Bundle()
//
//            args.putBoolean(ARG_FOR_MAP, isForMap)
//
//            val fragment = MoneyFragment()
//            fragment.arguments = args
//            return fragment
//        }
//    }
}
