package com.foxyteam.zaimbezotkazov.adapter


import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.foxyteam.zaimbezotkazov.R
import com.foxyteam.zaimbezotkazov.model.LoanModel
import org.jetbrains.anko.find

class LoansAdapter(private val callback: ((url: String?, title: String?, maxValue: String?,
                                           maxTerm: String?, percValue: String?, obtaineWay: String?,
                                           obtaineTime: String?, requirements: String?, location: String?, image: Int?) -> Unit)? = null) : RecyclerView.Adapter<LoansAdapter.MyViewHolder>() {

    private var itemsList: List<LoanModel> = listOf()

    fun setList(list: List<LoanModel>) {
        this.itemsList = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var loanLogo: ImageView = view.find(R.id.loanLogo)
        var text: TextView = view.find(R.id.text)
        var text2: TextView = view.find(R.id.text2)
        var text3: TextView = view.find(R.id.text3)
        var title: TextView = view.find(R.id.title)
        var term: TextView = view.find(R.id.term)
        var percentage: TextView = view.find(R.id.percentage)
        var textDetails: TextView = view.find(R.id.textDetails)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.loan_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = itemsList[position]

        val type = Typeface.createFromAsset(holder.itemView.context?.assets, "font/opensans_regular.ttf")
        val typeBold = Typeface.createFromAsset(holder.itemView.context?.assets, "font/opensans_bold.ttf")

        holder.text.typeface = type
        holder.text2.typeface = type
        holder.text3.typeface = type

        holder.title.typeface = typeBold
        holder.term.typeface = typeBold
        holder.percentage.typeface = typeBold
        holder.textDetails.typeface = typeBold

        holder.title.text = item.maxValue
        holder.term.text = item.maxTerm
        holder.percentage.text = item.percValue

        Glide.with(holder.itemView.context).load(item.image).into(holder.loanLogo)

        holder.textDetails.setOnClickListener {
            callback?.invoke(item.url, item.title, item.maxValue, item.maxTerm, item.percValue, item.obtaineWay, item.obtaineTime, item.requirements, item.location, item.image)
        }

    }

    override fun getItemCount(): Int {
        return itemsList.size
    }
}
